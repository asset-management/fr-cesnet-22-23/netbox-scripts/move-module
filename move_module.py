from extras.scripts import *
from dcim.models import Device, Location, ModuleBay, Module, Interface, PowerPort, PowerOutlet, ConsolePort, ConsoleServerPort, FrontPort, RearPort

__version__ = "0.1"
__author__ = "Michal Drobný"

# Timeout for the report run
JOB_TIMEOUT = 300

class MoveModule(Script):

    job_timeout = JOB_TIMEOUT

    class Meta:
        name = "Move Module"
        description = "Move Module to another device."
        field_order = ["source_location", "source_device", "source_module", "dest_location", "dest_device", "dest_module_bay"]

    source_location = ObjectVar(
        model=Location,
        required=False,
        label="Source Location"
    )
    source_device = ObjectVar(
        model=Device,
        required=True,
        label="Source Device",
        query_params={
            "location_id": "$source_location",
        }
    )
    source_module = ObjectVar(
        model=Module,
        required=True,
        label="Source Module",
        query_params={
            "device_id": "$source_device",
        },
    )
    dest_location = ObjectVar(
        model=Location,
        required=False,
        label="Destination Location",
    )
    dest_device = ObjectVar(
        model=Device,
        required=True,
        label="Destination Device",
        query_params={
            "location_id": "$dest_location",
        }
    )
    dest_module_bay = ObjectVar(
        model=ModuleBay,
        required=True,
        label="Destination Module Bay",
        query_params={
            "device_id": "$dest_device",
        }
    )

    def check_interfaces(self,source_device,source_module) -> bool:
        result = True
        for model in [Interface, PowerOutlet, PowerPort, ConsolePort,ConsoleServerPort,FrontPort,RearPort]:
            result &= self.check_all(source_device, source_module, model)
        return result


    def check_all(self, source_device, source_module, model):
        check_result = True
        device_interfaces = model.objects.filter(device=source_device)
        for device_interface in device_interfaces:
            if device_interface.inventory_items.filter(name__isnull=False) and device_interface.module == source_module:
                self.log_failure(f"[CHECK] The given Source Module has already connected inventory items. Please remove the inventory items from ({device_interface.name}).")
                check_result = False
            if device_interface.cable_id and device_interface.module == source_module:
                self.log_failure(f"[CHECK] The given Source Module has already connected cables. Please remove the cable from the interface ({device_interface.name}).")
                check_result = False
        return check_result
 
    def check_source_module(self, source_device, source_module):
        return source_module is not None and source_module.device == source_device
    
    def check_dest_module_bay_parent(self, dest_device, dest_module_bay):
        return dest_module_bay.device == dest_device

    def check_dest_module_bay_capacity(self, dest_device, dest_module_bay):
        return dest_module_bay.installed_module is None

    def run(self, data, commit):
        source_device = data.get("source_device")
        source_module = data.get("source_module")
        dest_device = data.get("dest_device")
        dest_module_bay = data.get("dest_module_bay")

        if self.check_interfaces(source_device,source_module) == False:
            return "Error" 
        self.log_success(f"[CHECK] The given Source Module (serial: {source_module.serial}) does not have any inventory items or cables attached.")    

        if not self.check_source_module(source_device, source_module):
            self.log_failure(f"[CHECK] The given Source Module (serial: {source_module.serial}) does not belong to the given Source Device ({source_device.name}).")
            return "Error"
        self.log_success(f"[CHECK] The given Source Module (serial: {source_module.serial}) belongs to the given Source Device ({source_device.name}).")

        if not self.check_dest_module_bay_parent(dest_device, dest_module_bay):
            self.log_failure(f"[CHECK] The given Destination Module Bay (serial: {source_module.serial}) does not belong to the given Destination Device ({source_device.name}).")
            return "Error"
        self.log_success(f"[CHECK] The given Destination Module Bay (serial: {source_module.serial}) belongs to the given Destination Device ({source_device.name}).")

        new_object = Module(
            device=dest_device,
            module_bay=dest_module_bay,
            module_type=source_module.module_type,
            description=source_module.description,
            comments=source_module.comments,
            status=source_module.status,
            serial=source_module.serial,
            asset_tag=source_module.asset_tag,
        )

        source_module.delete()
        self.log_success(f"Source Module has been deleted.")

        try:
            new_object.save()
            self.log_success(f"New module has been created.")
        except Exception as e:
            self.log_failure(f"Module was not moved due to the following error: {str(e)}")
            return "ERROR"
        
        self.log_info(f"BEFORE:")
        self.log_info(f"- device_name: {source_device.name}")
        self.log_info(f"- device_id: {source_device.id}")
        self.log_info(f"AFTER:")
        self.log_info(f"- device_name: {dest_device.name}")
        self.log_info(f"- device_id: {dest_device.id}")

        return "OK"
