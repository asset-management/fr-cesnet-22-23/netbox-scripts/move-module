# Move Module

## Popis
Tento skript je určen pro přesouvání modulů (Modules) mezi různými zařízeními.

## Instalace
- **[Verze 3.4.X a níže]** Skript je nutno vložit do složky scripts, která má nejčastěji tuto cestu netbox/scripts/.
- **[Verze 3.5.X a výše]** Skript je nutné naimportovat z lokální složky. Cesta v Netboxu je Customization -> Scripts -> Add
- Viz https://docs.netbox.dev/en/stable/customization/custom-scripts/

## Výstup
![](/images/move-module.gif)

## Prerekvizity
1. Vytvořen objekt Device
2. Vytvořen objekt Manufacturer
3. Vytvořen objekt Module Bay
4. Vytvořen objekt Module Type
5. Vytvořen objekt Module

## Postup fungování skriptu
1. Skript zkontroluje, zda má Source modul připojené na jeho interface-ech kabely, případně zda se na interface-ech nacházejí inventory items. Pokud ano, skript nahlásí chybu i s interface-em, který musí uživatel ošetřit. Neproběhnou žádné změny.
2. Skript zkontroluje, zda Source modul vážně patří Source device. Pokud ne, tak skončí a nebudou provedeny žádné změny.
3. Skript zkontroluje, zda Destination Module Bay patří Destination Device. Pokud ne, tak skončí a nebudou provedeny žádné změny.
4. Po projetí kontrol se vytvoří nový Module objekt, který obsahuje rovnaké atribúty jako Source Module.
5. Skript vymaže na Source device Source modul - pro tento krok je vytvořen log.
6. Následně skript uloží nový Module objekt na Destination Module Bay v Destination Device. V případě neznámé chyby, se uživateli vypíše chybové hlášení o této chybě a nebudou provedeny žádné změny.
7. Po úspěšném přesunutí modulu bude uživatel informován pomocí logů o předchozím stavu a stavu po přesunu.

## Proměnné
**Pro správné fungování skriptu je nutné upravit globální proměnné.**

#### JOB_TIMEOUT
- Táto proměnna představuje timeout pro běh skriptu.
- Výchozí hodnota je nastavena na 300 vteřin.
```python
JOB_TIMEOUT = 300
```

## Argumenty

#### Source Location
 - Nepovinný
 - Tento argument slouží především pro lepší hledání zdrojového zařízení (Source Device).

#### Source Device
 - Povinný
 - Tento argument představuje zařízení, ze kterého se modul bude přesouvat.

#### Source Module
 - Povinný
 - Tento argument představuje modul, který má být přesunut.

#### Destination Location
 - Nepovinný
 - Tento argument slouží především pro lepší hledání cílového zařízení (Destination Device).

#### Destination Device
 - Povinný
 - Tento argument představuje zařízení, na které se modul bude přesouvat.

#### Destination Module Bay
 - Povinný
 - Tento argument představuje Module Bay cílového zařízení, na který se modul bude přesouvat.
